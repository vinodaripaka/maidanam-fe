export class TeamModel {
    public teamId: number;
    public teamName: string;

    constructor(teamId: number, teamName: string) {
        this.teamId = teamId;
        this.teamName = teamName;
    }
}
